package com.school.project;

import java.util.Objects;

public class Meal {

    private int price;
    private String name;


    public Meal(int price) {
        this.price = price;
    }

    public Meal(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public Meal() {

    }

    public int getPrice() {
        return price;
    }

    public int getDiscountPrice(int discount){
        if(discount > price){
            throw new IllegalArgumentException("Discount can't be " +
                    "greater than price");
        }

        return this.price - discount;
    }



    public static void main(String[] args) {

        Meal burger = new Meal(10);
//        System.out.println(burger.getDiscountPrice(4)); // 6


        Meal meal = new Meal(10);
        Meal meal2 = new Meal(10);
        Meal meal3 = new Meal(20);

        System.out.println(meal.equals(meal2)); // true
        System.out.println(meal.equals(meal3)); // false



        
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meal)) return false;
        Meal meal = (Meal) o;
        return price == meal.price &&
                Objects.equals(name, meal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name);
    }
}
