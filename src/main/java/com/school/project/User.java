package com.school.project;

public class User {
    int userAge;
    String defaultName;

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultAge(int userAge) {
        if (userAge >= 18) {
            this.userAge = userAge;
        } else {
            throw new IllegalArgumentException("Age must be greater than 18");
        }
    }

    public void    setDefaultName(String defaultName) {
        if (defaultName.length() > 5) {
            this.defaultName = defaultName;
        } else {
            throw new IllegalArgumentException("Name size must be lower than 5 letters");

        }

    }



}
