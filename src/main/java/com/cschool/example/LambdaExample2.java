package com.cschool.example;

public class LambdaExample2 {


    public static void main(String[] args) {

        Concat concatAnnonymousClassInstance = str -> str.concat("abc");
        Concat concatAnnonymousClassInstance2 = str -> {
            String upperCase = str.toUpperCase();
            return str.concat(upperCase);
        };


//        //
//        System.out.println(com.cschool.example.SimpleCalculator.add(3,5));
//        //

        PowerOn powerOn = () ->{
            System.out.println(SimpleCalculator.add(3,5));
        };

//        powerOn.activate();


//        System.out.println(concatAnnonymousClassInstance.addAbc("DOM"));
//        System.out.println(concatAnnonymousClassInstance2.addAbc("kot"));


        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@





    }


}
