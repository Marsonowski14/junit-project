package com.cschool.example;

public class SumImpl implements Sum {
    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
