package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive() {
        // given
        Account account = new Account();

        //then
        assertFalse(account.isActive(),
                "Check if new account is not active");

        assertThat(account.isActive(), equalTo(false));
        assertThat(account.isActive(), is(false));

    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation() {
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActive(),
                "Check if new account is not active");

    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull() {

        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull(account1.getDefaultDeliverAddress());

        assertThat((account1.getDefaultDeliverAddress()), nullValue());
        ;


    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount() {

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress(new Address("Szkolna", "100a"));


        //then
        assertNotNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), notNullValue());

    }

    @Test
    void givenRightEmail() {
        //given
        Account account1 = new Account();
        //when
        account1.setEmailAdress("adrian@wp.pl");
        //then
        assertThat(account1.getEmailAdress(), notNullValue());
    }


    @Test
    void shouldPutThrows() {
        //givem
        Account account1 = new Account();

        //when
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAdress("adrian@wp.pl");
        });
    }

    @ParameterizedTest
    @ValueSource(strings = {"adrian.pl", "marcin.pl"})
    void correctEmaiShouldThrowExceptionIfIsToShortOrNotInclude(String str) {
        //given
        Account account1 = new Account();
        //when
        account1.setEmailAdress(str);
        //then
        assertThat(account1.getEmailAdress(), notNullValue());

    }

    @Test
    void method5() {
        //givem
        Account account1 = new Account();

//        account1.setEmailAdress("123124123421");
//        account1.setEmailAdress("1231241@gmail.com1");

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAdress("adrian@wp.pl");
             });
    }


}



