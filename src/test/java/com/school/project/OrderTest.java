package com.school.project;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class OrderTest {

    @Test
    void testAssertArrayEquals(){
        //given
        int[] ints1 = {1,2,3};
        int[] ints2 = {1,2,3};

        //then
        assertArrayEquals(ints1,ints2);
    }

    @Test
    void  mealListShouldBeEmptyAfterCreationOfOrder  (){
        Order order = new Order();

        //then
        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));


    }

    @Test
    void addingMealsToOrderShouldBeNotEmpty(){
        //given
        Meal meal = new Meal(3, "Kebap");
        Order order = new Order();

        //when
        order.addMealToOrder(meal);

        //then
        assertThat(order.getMeals(), hasSize(1));
    }

    @Test
    void method6() {
        Meal meal1 = new Meal(20, "pizza");
        Meal meal2 = new Meal(13, "burger");

        Order order = new Order();
        order.addMealToOrder(meal1);

        assertThat(order.getMeals(), contains(meal1));
        assertThat(order.getMeals(), not(contains(meal2))); //test will fail

        assertThat(order.getMeals(), hasItem(meal1));
        assertThat(order.getMeals().get(0).getPrice(), equalTo(10));
    }

        @Test
        void  removingMealFromOrderShouldDecreaseOrderSize(){
            //given
            Meal meal3 = new Meal(20, "pizza");
            Order order3 = new Order();
            order3.addMealToOrder(meal3);
            order3.removeFromMeal(meal3);

            //then
            assertThat(order3.getMeals(), not(contains(meal3)));
            assertThat(order3.getMeals(), empty());
            assertThat(order3.getMeals(), hasSize(0));

        }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder () {
        Meal meal1 = new Meal(20, "pizza");
        Meal meal2 = new Meal(13, "burger");

        Order order = new Order();

        order.addMealToOrder(meal1);
        order.addMealToOrder(meal2);

        assertThat(order.getMeals(), contains(meal1, meal2));
//            assertThat(order.getMeals(), containsInAnyOrder(meal1, meal2)); //test fails, wrong order of arguments
        assertThat(order.getMeals(), containsInAnyOrder(meal1, meal2));
        assertThat(order.getMeals(), hasItems(meal1, meal2));
        assertThat(order.getMeals(), hasItem(meal1));
    }

    @Test
    void testIfTwoOrdersAreSame(){
        Meal meal1 = new Meal(20, "pizza");
        Meal meal2 = new Meal(13, "burger");
        Meal meal3 = new Meal(13, "pierogi");

        List<Meal> mealList1 = Arrays.asList(meal1,meal2);
        List<Meal> mealList2 = Arrays.asList(meal1,meal2);
        List<Meal> mealList3 = Arrays.asList(meal2,meal3);
        List<Meal> mealList4 = Arrays.asList(meal1, meal2, meal3);

       Order order1 = new Order();
       Order order2 = new Order();
       Order order3 = new Order();

       order1.addMealToOrder(mealList1);
       order1.addMealToOrder(mealList2);
//       order1.addMealToOrder(mealList3); //homework make this scenario pass test

        assertThat(order1.getMeals(), is(order2.getMeals()));



    }


}

