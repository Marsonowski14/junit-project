package com.school.project;

//import org.graalvm.compiler.lir.LIRInstruction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    //1
    @Test
    void method1() {
        User user = new User();

        assertThat((user.getDefaultName()), nullValue());
    }

    @Test
    void method2() {
        User userAge = new User();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            userAge.setDefaultAge(12);
        });
    }
        // Account account1 = new Account();
        // Assertions.assertThrows(IllegalArgumentException.class, () -> {
        //        account1.setEmailAdress("adrian@wp.pl");
        //
        //
        //}

    @ParameterizedTest
    @ValueSource(strings = {"Ola", "Ala", "Leo"})
    void method3(String str){
        User name = new User();

        name.setDefaultName(str);

        assertThat(name.getDefaultName(), notNullValue());

    }


//    @ParameterizedTest
//    @ValueSource(strings = {"adrian.pl", "marcin.pl"})
//    void correctEmaiShouldThrowExceptionIfIsToShortOrNotInclude(String str) {
//        //given
//        Account account1 = new Account();
//        //when
//        account1.setEmailAdress(str);
//        //then
//        assertThat(account1.getEmailAdress(), notNullValue());
//
//    }
}




